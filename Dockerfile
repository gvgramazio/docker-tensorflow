FROM ubuntu:16.04
LABEL maintainer="Giuseppe Valerio Gramazio <gvgramazio@gmail.com>"

# install apt packages
RUN apt-get update
RUN apt-get install -y \
  python3-pip \
  htop \
  nano \
  git \
  wget \
  libglib2.0-0 \
  ffmpeg

# install python modules
ADD requirements.txt /
RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install -r requirements.txt

# setup juptyer
RUN jupyter nbextension enable --py --sys-prefix widgetsnbextension
RUN jupyter contrib nbextension install --user
RUN jupyter nbextension enable codefolding/main
RUN echo "c.NotebookApp.ip = '*'" >> /root/.jupyter/jupyter_notebook_config.py
RUN echo "c.NotebookApp.port = 8080" >> /root/.jupyter/jupyter_notebook_config.py
RUN echo "c.NotebookApp.token = ''" >> /root/.jupyter/jupyter_notebook_config.py

COPY notebooks /notebooks
WORKDIR /notebooks
EXPOSE 8080
CMD ["jupyter", "notebook", "--no-browser", "--allow-root"]
